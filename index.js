const express = require("express");
const converter = require("./converter");

const app = express();

app.get("/inchToCm", (req, res) => {
  const inches = parseFloat(req.query.inches);
  const cm = converter.inchToCm(inches);
  let payload = {};
  payload.unit = "cm";
  payload.value = cm;
  res.send(payload);
});

app.get("/cmToInch", (req, res) => {
  const cm = parseFloat(req.query.cm);
  const inches = converter.cmToInch(cm);
  let payload = {};
  payload.unit = "inch";
  payload.value = inches;
  res.send(payload);
});

const port = process.env.PORT || 8080;
let server = app.listen(port, () =>
  console.log(`Server running on port ${port}`),
)

exports.server = server;
exports.express_app = app;