const expect = require("chai").expect;
const converter = require("../converter");

describe("Unit Converter", () => {
  describe("Metric to US conversion", () => {
    it("converts from the metric system to the US/UK system", () => {
      const foot = converter.meterToFoot(1);
      const inch = converter.cmToInch(1);

      const expectedFoot = 3.280839895;
      const expectedInch = 0.3937007874;
      //expect(foot).to.equal(expectedFoot);
      //expect(inch).to.equal(expectedInch);
      const delta = 0.0001;
      expect(foot).approximately(expectedFoot, delta);
      expect(inch).approximately(expectedInch, delta);
    });
  });

  describe("US to metric conversion", () => {
    it("converts from the US/UK system to the metric system", () => {
      const meter = converter.footToMeter(1);
      const cm = converter.inchToCm(1);

      expect(meter).to.equal(0.3048);
      expect(cm).to.equal(2.54);
    });
  });
});

const request = require("supertest");


describe("Unit Converter Web Server", () => {
  describe("GET method", () => {
    it('should get 1 inch in cm', (done) => {
      const server = require("../index").server;
      const app = require("../index").express_app;
      request(app)
        .get('/inchToCm?inches=1')
        .expect(200)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .end((err, res) => {
          expect(res.body.unit).to.equal('cm');
          expect(res.body.value).to.equal(2.54);
          server.close(done);
        })
    });
  });
});
