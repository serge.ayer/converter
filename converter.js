exports.cmToInch = (cm) => {
  return cm / 2.54;
};

exports.inchToCm = (inches) => {
  return inches * 2.54;
};

exports.meterToFoot = (m) => {
  return m / 0.3048;
};

exports.footToMeter = (feet) => {
  return feet * 0.3048;
};
